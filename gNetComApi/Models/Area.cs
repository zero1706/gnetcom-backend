﻿using System;
using System.Collections.Generic;

namespace gNetComApi.Models
{
    public partial class Area
    {
        public int Id { get; set; }
        public string? Nombre { get; set; }
        
        public string ? Image { get; set; }
    }
}
