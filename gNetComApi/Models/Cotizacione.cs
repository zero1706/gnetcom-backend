﻿using System;
using System.Collections.Generic;

namespace gNetComApi.Models
{
    public partial class Cotizacione
    {
        public int Id { get; set; }
        public int? IdCliente { get; set; }
        public int? IdProducto { get; set; }
        public bool? FlagEstado { get; set; }

        public virtual Producto? IdProductoNavigation { get; set; }
    }
}
