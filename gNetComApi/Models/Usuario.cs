﻿using System;
using System.Collections.Generic;

namespace gNetComApi.Models
{
    public partial class Usuario
    {
        public int Id { get; set; }
        public string? Nombre { get; set; }
        public string? Apellido { get; set; }
        public string? Descripcion { get; set; } 
        public string? Imagen { get; set; }
    }
}
