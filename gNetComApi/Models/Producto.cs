﻿using System;
using System.Collections.Generic;

namespace gNetComApi.Models
{
    public partial class Producto
    {
        public Producto()
        {
            Cotizaciones = new HashSet<Cotizacione>();
        }

        public int Id { get; set; }
        public string? Nombre { get; set; }
        public string? Precio { get; set; }

        public string? Imagen { get; set; }

        public virtual ICollection<Cotizacione> Cotizaciones { get; set; }
    }
}
