﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using gNetComApi.Models;
using Microsoft.EntityFrameworkCore;

namespace gNetComApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly gNetComContext _db;

        public UsuarioController(gNetComContext db)
        {
            _db = db;
        }

        [HttpGet]
        [Route("listUsuarios")]
        public async Task<IActionResult> GetUsuarios()
        {
            var lista = await _db.Usuarios.ToListAsync();
            return Ok(lista);
        }

        [HttpGet]
        [Route("listClientes")]
        public async Task<IActionResult> GetClientes()
        {
            var lista = await _db.Clientes.ToListAsync();

            return Ok(lista);
        }
        [HttpGet]
        [Route("listAreas")]
        public async Task<IActionResult> getAreas()
        {
            var lista = await _db.Areas.ToListAsync();

            return Ok(lista);
        }

        [HttpGet("/{id}")]
        public async Task<IActionResult> GetUsuarioId(int id)
        {
            Usuario usuario = new Usuario();
            usuario.Id = id;
            var validar = await _db.Usuarios.Where(x => x.Id == usuario.Id).ToListAsync();
            if (validar.Count() > 0)
            {
                return Ok(validar.FirstOrDefault());
            }
            return BadRequest(ModelState);
        }
    }
}
